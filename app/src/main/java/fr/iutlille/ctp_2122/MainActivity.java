package fr.iutlille.ctp_2122;  // ligne à modifier


import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import fr.iutlille.ctp_2122.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private final int active = 0;
    private ActivityMainBinding ui;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ui = ActivityMainBinding.inflate(this.getLayoutInflater());
        setContentView(ui.getRoot());
        ParcoursBUT application = (ParcoursBUT) getApplication();
        /* TODO Q1: statistiques */
        showStats(application);

        CandidatureAdapter adapter = new CandidatureAdapter(application.getCandidatures());
        ui.recyclerView.setAdapter(adapter);
        RecyclerView.LayoutManager lm = new LinearLayoutManager(this);
        ui.recyclerView.setLayoutManager(lm);
        /* TODO Q3a: Menu contextuel */
        /* TODO Q3: RecyclerView */
    }

    /* TODO Q1: statistiques */

    public void showStats(ParcoursBUT application){
        List<Candidature> list = application.getCandidatures();
        TextView titre = findViewById(R.id.Title);
        titre.setText(list.size() + " prospects");
        int nbNope = 0;
        int nbTodo = 0;
        int nbDone = 0;
        int nbLate = 0;
        for (int i = 0; i < list.size(); i++) {
            Candidature candidature = list.get(i);
            String etat = candidature.getEtat().name();
            if(etat.equals("TODO")){
                nbTodo++;
            } else if (etat.equals("NOPE")){
                nbNope++;
            } else if (etat.equals("DONE")){
                nbDone++;
            } else if (etat.equals("LATE")){
                nbLate++;
            }
        }
        TextView nope = findViewById(R.id.nbNope);
        TextView todo = findViewById(R.id.nbTodo);
        TextView done = findViewById(R.id.nbDone);
        TextView late = findViewById(R.id.nbLate);
        nope.setText(nbNope +"");
        todo.setText(nbTodo+ "");
        done.setText(nbDone+ "");
        late.setText(nbLate+ "");

    }

    /* TODO Q2: A propos */

    public void clickAbout(View btn){
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    /* TODO Q3a: Menu contextuel */

    /* TODO Q3b: Tri de liste */

}