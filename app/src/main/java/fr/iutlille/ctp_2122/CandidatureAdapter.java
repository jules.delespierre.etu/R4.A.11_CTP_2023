package fr.iutlille.ctp_2122;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import fr.iutlille.ctp_2122.databinding.ItemCandidatureBinding;

/* TODO Q3: RecyclerView */
public class CandidatureAdapter extends RecyclerView.Adapter<CandidatureViewHolder> {
    private final List<Candidature> list;

    public CandidatureAdapter(List<Candidature> list){
        this.list = list;
    }

    @NonNull
    @Override
    public CandidatureViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCandidatureBinding binding = ItemCandidatureBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new CandidatureViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CandidatureViewHolder holder, int position) {
        Candidature candidature = list.get(position);
        holder.setCandidature(candidature);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

    /* TODO Q3a: Menu contextuel */
    /* TODO Q5: EditItem */
