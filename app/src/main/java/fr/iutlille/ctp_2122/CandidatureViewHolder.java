package fr.iutlille.ctp_2122;

/* TODO Q3: RecyclerView */

import androidx.recyclerview.widget.RecyclerView;

import java.util.Date;

import fr.iutlille.ctp_2122.databinding.ActivityCandidatureBinding;
import fr.iutlille.ctp_2122.databinding.ActivityMainBinding;
import fr.iutlille.ctp_2122.databinding.ItemCandidatureBinding;

public class CandidatureViewHolder extends RecyclerView.ViewHolder {
    public ItemCandidatureBinding ui;

    public CandidatureViewHolder(ItemCandidatureBinding ui){
        super(ui.getRoot());
        this.ui = ui;
    }

    public void setCandidature(Candidature candidature){
        ui.etCandidature.setText(candidature.getNom());
        Date date = candidature.getLimite();
        ui.dateLimite.setText(date.getDay() + "/" + date.getMonth() + "/" + (date.getYear()+1900));
        String etat = candidature.getEtat().name();
        if(etat.equals("TODO")){
            ui.status.setImageResource(R.drawable.warning);
        } else if (etat.equals("DONE")) {
            ui.status.setImageResource(R.drawable.thumb_up);
        } else if (etat.equals("LATE")) {
            ui.status.setImageResource(R.drawable.alarm);
        } else if (etat.equals("NOPE")) {
            ui.status.setImageResource(R.drawable.layers_clear);
        }
    }
}
    /* TODO Q3a: Menu contextuel */
    /* TODO Q5: EditItem */

