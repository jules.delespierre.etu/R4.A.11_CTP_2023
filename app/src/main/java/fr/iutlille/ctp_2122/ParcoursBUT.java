package fr.iutlille.ctp_2122;

import android.app.Application;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ParcoursBUT extends Application {
    private List<Candidature> candidatures;

    public List<Candidature> getCandidatures() {
        return candidatures;
    }

    public void onCreate() {
        super.onCreate();
        try {
            candidatures = Candidature.LoadCandidaturesFromResources(this, R.raw.cand);
        } catch (JSONException | IOException e) {
            Toast failedLoad = Toast.makeText(this, R.string.failedLoad, Toast.LENGTH_LONG);
            failedLoad.show();
            candidatures = new ArrayList<>();
        }
    }

    private static class DateComparator implements Comparator<Candidature> {
        @Override
        public int compare(Candidature o1, Candidature o2) {
            return o1.getLimite().compareTo(o2.getLimite());
        }
        public boolean equals(Object o) {
            return o instanceof DateComparator;
        }
    }
    private static class StatusComparator implements Comparator<Candidature> {
        @Override
        public int compare(Candidature o1, Candidature o2) {
            return o1.getEtat().compareTo(o2.getEtat());
        }
        public boolean equals(Object o) {
            return o instanceof StatusComparator;
        }
    }
    private final static DateComparator DATE_COMPARATOR = new DateComparator();
    private final static StatusComparator STATUS_COMPARATOR = new StatusComparator();

    public void setStatus(int position, Candidature.Etat newStatus) {
        this.candidatures.get(position).setEtat(newStatus);
    }

    public void sortByDate() {
        candidatures.sort(DATE_COMPARATOR);
    }

    public void sortByStatus() {
        candidatures.sort(STATUS_COMPARATOR);
    }
}
